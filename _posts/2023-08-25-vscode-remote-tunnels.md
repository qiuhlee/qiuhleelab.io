---
layout: post
title: "使用VSCode插件Remote-Tunnels连接服务器"
author: "qiuhlee"
header-style: text
tags: [VS Code, Tunnels]
excerpt: 本方法旨在从本地VSCode对远程服务器代码进行调试，以 linux 服务器为例，项目位于远程内网服务器，通过VSCode 同步远程服务器环境并在本地调试服务器代码（注意：无需将远程服务器代码同步到本地）。
---

本方法旨在从本地 VSCode 对远程服务器代码进行调试，若进行长时间的训练模型，为避免网络不稳定导致训练中断，建议使用 XRDP 或 VNC 登录到容器中运行。

以 Linux 服务器为例，项目位于远程内网服务器，通过 VSCode 同步远程服务器环境并在本地调试服务器代码（注意：无需将远程服务器代码同步到本地）。

VSCode提供了两种连接服务器的方法，分别使用`Remote - Tunnels`和`Remote - SSH`插件。本文介绍使用`Remote - Tunnels`连接服务器，该方法需要远程服务器有流畅的网络环境，它使用微软的服务来建立隧道，需要登录GitHub。欲使用`Remote - SSH`连接服务器可参考[外网/内网下使用VSCode插件Remote-SSH连接服务器](https://qiuhlee.gitlab.io/2023/08/25/vscode-remote-ssh/)。

使用`Remote - Tunnels`扩展有两种方法。可以使用命令行接口(CLI)，也可以通过VSCode桌面用户界面(UI)启用隧道。这两种方法提供了相同的隧道功能。

### 在远程服务器设置安全隧道

#### 使用命令行接口(CLI)启用隧道

此方法无需在远程服务器上安装完整的VSCode桌面版, 只需下载安装code CLI独立安装包。code CLI会在你的客户端和远程机器之间建立一个安全的隧道。

```bash
# 选定下载目录，如：
mkdir /home/dev/bin && cd /home/dev/bin
# 下载code CLI
curl -Lk 'https://code.visualstudio.com/sha/download?build=stable&os=cli-alpine-x64' --output vscode_cli.tar.gz
# 解压code CLI
tar -xf vscode_cli.tar.gz # 解压可得到可执行文件code
# 将code加入环境变量
vi ~/.bashrc # 在文件末尾加入下方内容
################################
export PATH=/home/dev/bin:$PATH
################################
source ~/.bashrc # 刷新环境变量使配置生效
# 创建安全隧道
code tunnel --name 3090_6_docker # --name 指定服务名称
```
通过上述code tunnel命令，将在远程服务器上下载并启动VSCode Server并创建安全隧道，获得一个与此远程服务器相关联的vscode.dev URL。
通过查看服务状态，如果出现错误，根据提示重新赋权。
![](/post_img/remote-ssh/remote-ssh01.png)

> 也可以前往[独立安装界面](https://code.visualstudio.com/#alt-downloads)下载并安装code tunnel
![](/post_img/remote-ssh/remote-ssh03.png)
如果未添加环境变量，后续的命令需要以`./code`替代`code`

#### 使用完整的VSCode桌面版启用隧道

此方法需首先在远程服务器上安装完整的VSCode桌面版，具体启用方法如下。
在远程服务器上的VSCode账户菜单中，选择“Turn on Remote Tunnel Access”选项。

![](/post_img/remote-ssh/remote-ssh00.png)

此方法同样会获得一个与此远程服务器相关联的vscode.dev URL。

### 在本地客户端连接安全隧道
    
点击左侧的扩展按钮(或用 Ctrl+Shift+X)，搜索插件`Remote - Tunnels`进行安装

![](/post_img/remote-ssh/remote-ssh04.png)

按照下方提示登录GitHub查看注册的隧道

![](/post_img/remote-ssh/remote-ssh02.png)

按照下方提示即可连接到远程服务器

![](/post_img/remote-ssh/remote-ssh05.png)