---
layout: post
title: "Docker使用常用命令"
author: "heleiqiu"
header-style: text
tags: [Docker, 终端]
excerpt: 介绍Docker容器及其相关配置，容器模型训练可视化。
---

## 一. Docker容器及其相关配置

### 拉取、运行镜像

```bash
sudo docker Docker镜像名字    # 1.69 GB
sudo docker run -itd -p 8022:22 --gpus all --shm-size 4G --name 容器名 -v /服务主机路径:/容器路径 Docker镜像名字
sudo docker exec -t -i 容器名 /bin/bash                    # 进入容器
```

### 更换清华源

```bash
apt update                                                        # 更新列表
mv /etc/apt/sources.list /etc/apt/sources.list.backup             # 备份Ubuntu源
vim /etc/apt/sources.list                                         # 更换清华源，写入如下内容

******************************************************************************
# 源地址查询：https://mirror.tuna.tsinghua.edu.cn/help/ubuntu/
# Ubuntu 22.04 LTS软件仓库镜像
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ jammy main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ jammy-updates main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ jammy-backports main restricted universe multiverse
deb http://security.ubuntu.com/ubuntu/ jammy-security main restricted universe multiverse
******************************************************************************

apt update                                                        # 更新列表
```

### 安装pip3并更换国内源

```bash
# 1. 安装pip3
apt install python3-pip   
                                        
# 2. 建立配置文件
若为Linux系统，在主目录新建.pip文件夹，在.pip目录新建pip.conf
cd ~ & mkdir .pip & cd .pip & vim pip.conf                        
若为Windows系统，在文件管理器中输入%APPDATA%，进入新建pip，进入新建pip.ini

# 3. 打开并填入如下内容
************************************************************
[global]
index-url = https://pypi.tuna.tsinghua.edu.cn/simple
************************************************************
```

4. 设置python3和pip3为默认

```bash
ln -sf /usr/bin/python3.8 /usr/bin/python
ln -sf /usr/bin/pip3 /usr/bin/pip
```

### 设置SSH安装及自启动

- 安装SSH服务

```bash
apt install openssh-server                                        # 安装SSH服务
echo 'root:qiu' | chpasswd                                        # 修改root账号密码为“qiu”
# 修改ssh登陆root账号许可
sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd
echo "export VISIBLE=now" >> /etc/profile
service ssh restart                                               # 重启ssh服务
# 在SSH连接中使用环境变量
vim /etc/profile                                                  # 编辑profile，填入如下内容
export $(cat /proc/1/environ |tr '\0' '\n' | xargs)
```

- ssh开机自启动

```bash
cd run
vim run.sh                                                        # 创建自启动脚本，填入如下内容并保存退出文件

********************
#!/bin/sh
service ssh start
/bin/bash
********************

chmod 777 run.sh                                                  # 提升自启动脚本的权限
```

### 以新镜像创建一个容器，并在代码最后加上/run/run.sh

```bash                              
sudo docker run -itd -p 8022:22 --gpus all --shm-size 4G --name 容器名 -v /服务主机路径:/容器路径 Docker镜像名 /run/run.sh
```

### 容器的查看、保存、停止、删除、启动、创建

- Ctrl+D退出容器，列出所有容器

```bash
sudo docker ps -a                                 # 列出所有容器
sudo docker commit [容器ID] ubuntu:pytorch        # 将新容器保存为新的镜像
sudo docker stop [容器ID]                         # 停止容器
sudo docker rm [容器ID]                           # 删除容器
sudo docker start [容器ID]                        # 启动容器
```

### 镜像的查看、删除、保存、迁移、加载

```bash
sudo docker image ls                             # 查看镜像
sudo docker rmi Docker镜像名                      # 删除镜像
sudo docker save ImageID > xxx.tar               # 保存镜像
sudo scp image.tar 另一主机用户名@IP:/目标路径       # 迁移镜像
sudo docker load < xxx.tar                       # 加载镜像
```