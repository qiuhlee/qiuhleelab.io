---
layout: post
title: "终端常用命令"
author: "heleiqiu"
header-style: text
tags: [终端]
excerpt: 记录文件解压缩，解决向日葵无法连Ubuntu，ubuntu+jekyll搭建个人博客等命令。
---

## 一. Conda命令
#### Conda常用命令
 ```bash
查看环境：conda info --env
检查更新：conda update conda
创建环境：conda create -n new_env_name python=x.x
进入环境：conda activate env_name (Window) 或 source activate env_name (Linux/Mac)
退出环境：conda deactivate
删除环境：conda env remove -n env_name
查看库包：conda list
搜索库包：conda search package_name
安装库包：conda install package_name 或 conda install -n env_name package_name
删除库包：conda remove package_name 或 conda remove -n env_name package_name
更新库包：conda update package_name 或 conda update -n env_name package_name
 ```

#### 添加Conda镜像
 ```bash
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/r/
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/pytorch/
conda config --set show_channel_urls yes
```

#### Conda安装32位Python：
```bash
1. 设置当前环境为32位
conda config --env --set subdir win-32 #'linux-32', 'win-32'等，可以用conda info查看环境是否变为32位
2. 搜索conda中包含的安装包版本
搜索库包：conda search package_name --platform win-32 #'osx-64', 'linux-32', 'win-64', and so on
3. 在当前32位环境下创建新环境
conda create -n new_env_name python=x.x # 可以用python -c "import platform; print(platform.architecture()[0])"查看Python是否变为32位
```

## 二. 文件解压缩

1. 检查僵尸进程
```bash
top  # 当zombie前的数量不为0时，即系统内存在相应数量的僵尸进程。
```

2. 查询父进程
```bash
ps -A -ostat,ppid,pid,cmd |grep -e '^[Zz]'
```

3. 通过父进程杀死僵尸进程
```bash
kill -HUP 僵尸进程父ID
```

## 三. 文件解压缩

#### 压缩

```bash
tar -czvf filename.tar.gz /xxx/folder
```

#### 分割

```bash
split -b 4000M -a 2 -d filename.tar.gz filename.tar.gz. –verbose
# 命令最后的一个点"."不能丢了
# -b意思是切分片大小，其单位可以为b（512字节）、k（1K）、m（1M）
# -d 是切分后的包命名为*.tar.gz.00 *.tar.gz.01等这种数字结尾v
# -a X 指定后缀名的长度，默认为2位
#  --verbose 详细的列出处理的文件
```

#### 连接文件为单独一个

```bash
cat filename.tar.gz.a* >> filename.tar.gz
```

#### 解压文件

```bash
tar -xzvf filename.tar.gz -C /data/
```

## 四. 修改Linux终端的默认路径

打开`/home/your_username/.bashrc`文件（此文件是隐藏的），添加一行代码
```bash
cd destination_path
```
destination_path既是你想设置的终端默认打开路径
