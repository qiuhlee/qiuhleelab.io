---
layout: post
title: "TensorFlow下利用MNIST训练模型识别手写数字"
author: "heleiqiu"
header-style: text
tags: [MNIST, TensorFlow, 数字识别]
excerpt: 本文将参考TensorFlow中文社区官方文档使用mnist数据集训练一个多层卷积神经网络LeNet5网络，并利用所训练的模型识别自己手写数字。
---

本文将参考[TensorFlow中文社区官方文档](http://tensorfly.cn/tfdoc/tutorials/mnist_pros.html)使用mnist数据集训练一个多层卷积神经网络([LeNet5网络](http://cuijiahua.com/blog/2018/01/dl_3.html))，并利用所训练的模型识别自己手写数字。
## 训练MNIST

```python
# Python3

# 使用LeNet5的七层卷积神经网络用于MNIST手写数字识别

import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

mnist = input_data.read_data_sets("MNIST_data", one_hot=True)

# 为输入图像和目标输出类别创建节点
x = tf.placeholder(tf.float32, shape=[None, 784]) # 训练所需数据  占位符
y_ = tf.placeholder(tf.float32, shape=[None, 10]) # 训练所需标签数据  占位符

# *************** 构建多层卷积网络 *************** #

# 权重、偏置、卷积及池化操作初始化,以避免在建立模型的时候反复做初始化操作
def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1) # 取随机值，符合均值为0，标准差stddev为0.1
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)

# x 的第一个参数为图片的数量，第二、三个参数分别为图片高度和宽度，第四个参数为图片通道数。
# W 的前两个参数为卷积核尺寸，第三个参数为图像通道数，第四个参数为卷积核数量
# strides为卷积步长，其第一、四个参数必须为1，因为卷积层的步长只对矩阵的长和宽有效
# padding表示卷积的形式，即是否考虑边界。"SAME"是考虑边界，不足的时候用0去填充周围，"VALID"则不考虑
def conv2d(x, W):
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

# x 参数的格式同tf.nn.conv2d中的x，ksize为池化层过滤器的尺度，strides为过滤器步长
def max_pool_2x2(x):
  return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

#把x更改为4维张量，第1维代表样本数量，第2维和第3维代表图像长宽， 第4维代表图像通道数  
x_image = tf.reshape(x, [-1,28,28,1]) # -1表示任意数量的样本数,大小为28x28，深度为1的张量

# 第一层：卷积
W_conv1 = weight_variable([5, 5, 1, 32]) # 卷积在每个5x5的patch中算出32个特征。
b_conv1 = bias_variable([32])

h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1) 

# 第二层：池化
h_pool1 = max_pool_2x2(h_conv1)

# 第三层：卷积
W_conv2 = weight_variable([5, 5, 32, 64])
b_conv2 = bias_variable([64])

h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)

# 第四层：池化
h_pool2 = max_pool_2x2(h_conv2)

# 第五层：全连接层
W_fc1 = weight_variable([7 * 7 * 64, 1024])
b_fc1 = bias_variable([1024])

h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

# 在输出层之前加入dropout以减少过拟合
keep_prob = tf.placeholder("float")
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

# 第六层：全连接层
W_fc2 = weight_variable([1024, 10])
b_fc2 = bias_variable([10])

# 第七层：输出层
y_conv=tf.nn.softmax(tf.matmul(h_fc1_drop, W_fc2) + b_fc2)

# *************** 训练和评估模型 *************** #

# 为训练过程指定最小化误差用的损失函数，即目标类别和预测类别之间的交叉熵
cross_entropy = -tf.reduce_sum(y_*tf.log(y_conv))

# 使用反向传播，利用优化器使损失函数最小化
train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)

# 检测我们的预测是否真实标签匹配(索引位置一样表示匹配)
# tf.argmax(y_conv,dimension), 返回最大数值的下标 通常和tf.equal()一起使用，计算模型准确度
# dimension=0 按列找  dimension=1 按行找
correct_prediction = tf.equal(tf.argmax(y_conv,1), tf.argmax(y_,1))  

# 统计测试准确率， 将correct_prediction的布尔值转换为浮点数来代表对、错，并取平均值。
accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))

saver = tf.train.Saver() # 定义saver

# *************** 开始训练模型 *************** #
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    for i in range(1000):
      batch = mnist.train.next_batch(50)
      if i%100 == 0:
        # 评估模型准确度，此阶段不使用Dropout
        train_accuracy = accuracy.eval(feed_dict={x:batch[0], y_: batch[1], keep_prob: 1.0})
        print("step %d, training accuracy %g"%(i, train_accuracy))

      # 训练模型，此阶段使用50%的Dropout
      train_step.run(feed_dict={x: batch[0], y_: batch[1], keep_prob: 0.5}) 

    saver.save(sess, './save/model.ckpt') #模型储存位置

    print("test accuracy %g"%accuracy.eval(feed_dict={x: mnist.test.images [0:2000], y_: mnist.test.labels [0:2000], keep_prob: 1.0}))
```

## 手写数字图像预处理
然后自己手写数字

![](/post_img/mnist_tensorflow/1.jpg)

利用Python和OpenCV进行图像预处理

>需要安装Python的OpenCV接口(安装命令：pip install opencv-python)
MNIST要求数据为28*28像素，单通道，且需要二值化。

```python
import cv2

global img
global point1, point2
def on_mouse(event, x, y, flags, param):
    global img, point1, point2
    img2 = img.copy()
    if event == cv2.EVENT_LBUTTONDOWN:         #左键点击
        point1 = (x,y)
        cv2.circle(img2, point1, 10, (0,255,0), 5)
        cv2.imshow('image', img2)
    elif event == cv2.EVENT_MOUSEMOVE and (flags & cv2.EVENT_FLAG_LBUTTON):   #按住左键拖曳
        cv2.rectangle(img2, point1, (x,y), (255,0,0), 5) # 图像，矩形顶点，相对顶点，颜色，粗细
        cv2.imshow('image', img2)
    elif event == cv2.EVENT_LBUTTONUP:         #左键释放
        point2 = (x,y)
        cv2.rectangle(img2, point1, point2, (0,0,255), 5) 
        cv2.imshow('image', img2)
        min_x = min(point1[0], point2[0])     
        min_y = min(point1[1], point2[1])
        width = abs(point1[0] - point2[0])
        height = abs(point1[1] -point2[1])
        cut_img = img[min_y:min_y+height, min_x:min_x+width]
        resize_img = cv2.resize(cut_img, (28,28)) # 调整图像尺寸为28*28
        ret, thresh_img = cv2.threshold(resize_img,127,255,cv2.THRESH_BINARY) # 二值化
        cv2.imshow('result', thresh_img)
        cv2.imwrite('./images/text.png', thresh_img)  # 预处理后图像保存位置

def main():
    global img
    img = cv2.imread('./images/src.jpg')  # 手写数字图像所在位置
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) # 转换图像为单通道(灰度图)
    cv2.namedWindow('image')
    cv2.setMouseCallback('image', on_mouse) # 调用回调函数
    cv2.imshow('image', img)
    cv2.waitKey(0)

if __name__ == '__main__':
    main()
```

运行上方代码，利用鼠标框选自己手写数字区域，完成图像预处理，可得到如下图像
![](/post_img/mnist_tensorflow/2.png)



##手写数字识别
完成图像预处理后，即可将图片输入到网络中进行识别

```python

from PIL import Image
import tensorflow as tf
import numpy as np

im = Image.open('./images/text.png')
data = list(im.getdata())
result = [(255-x)*1.0/255.0 for x in data] 
print(result)

# 为输入图像和目标输出类别创建节点
x = tf.placeholder("float", shape=[None, 784]) # 训练所需数据  占位符

# *************** 构建多层卷积网络 *************** #
def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1) # 取随机值，符合均值为0，标准差stddev为0.1
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)

def conv2d(x, W):
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_2x2(x):
  return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

x_image = tf.reshape(x, [-1,28,28,1]) # -1表示任意数量的样本数,大小为28x28，深度为1的张量

W_conv1 = weight_variable([5, 5, 1, 32]) # 卷积在每个5x5的patch中算出32个特征。
b_conv1 = bias_variable([32])
h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1) 
h_pool1 = max_pool_2x2(h_conv1)

W_conv2 = weight_variable([5, 5, 32, 64])
b_conv2 = bias_variable([64])
h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
h_pool2 = max_pool_2x2(h_conv2)

W_fc1 = weight_variable([7 * 7 * 64, 1024])
b_fc1 = bias_variable([1024])
h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

# 在输出层之前加入dropout以减少过拟合
keep_prob = tf.placeholder("float")
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

# 全连接层
W_fc2 = weight_variable([1024, 10])
b_fc2 = bias_variable([10])

# 输出层
# tf.nn.softmax()将神经网络的输层变成一个概率分布
y_conv=tf.nn.softmax(tf.matmul(h_fc1_drop, W_fc2) + b_fc2)

saver = tf.train.Saver() # 定义saver

# *************** 开始识别 *************** #
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    saver.restore(sess, "./save/model.ckpt")#这里使用了之前保存的模型参数

    prediction = tf.argmax(y_conv,1)
    predint = prediction.eval(feed_dict={x: [result],keep_prob: 1.0}, session=sess)

    print("recognize result: %d" %predint[0])
```

运行上述程序，即可得到识别结果，如下图所示：
![](/post_img/mnist_tensorflow/3.jpg)


可以看到识别的结果为3, 大功告成！