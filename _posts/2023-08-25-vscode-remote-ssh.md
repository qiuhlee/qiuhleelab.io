---
layout: post
title: "外网/内网下使用VSCode插件Remote-SSH连接服务器"
author: "qiuhlee"
header-style: text
tags: [VS Code, SSH]
excerpt: 本方法旨在从本地VSCode对远程服务器代码进行调试。以 linux 服务器为例，项目位于远程内网服务器，通过VSCode 同步远程服务器环境并在本地调试服务器代码（注意：无需将远程服务器代码同步到本地）。
---


本方法旨在从本地 VSCode 对远程服务器代码进行调试，若进行长时间的训练模型，为避免网络不稳定导致训练中断，建议使用 XRDP 或 VNC 登录到容器中运行。

以 Linux 服务器为例，项目位于远程内网服务器，通过 VSCode 同步远程服务器环境并在本地调试服务器代码（注意：无需将远程服务器代码同步到本地）。

### VSCode连接服务器

VSCode提供了两种连接服务器的方法，分别使用`Remote - SSH`和`Remote - Tunnels`插件。本文介绍使用`Remote - SSH`连接服务器。需要注意，使用`Remote - SSH`外网客户端访问内网服务器需要中转机作为跳转，此处不再介绍如何设置中转机。若无中转机，可以使用`Remote - Tunnels`连接服务器，使用方法可参考[VSCode使用插件Remote-Tunnels连接服务器](https://qiuhlee.gitlab.io/2023/08/25/vscode-remote-tunnels/)。

1. 安装 Remote - SSH 插件

    点击左侧的扩展按钮(或用 Ctrl+Shift+X)，搜索插件`Remote - SSH`进行安装

    ![](/post_img/remote-ssh/remote-ssh1.png)

2. 打开 Remote - SSH 配置文件

    安装完后会在左侧生成远程连接的图标，点击此图标，然后选择设置图标

    ![](/post_img/remote-ssh/remote-ssh2.png)

    点击弹出的`config`文件，在文件中进行配置。

    ![](/post_img/remote-ssh/remote-ssh3.png)

3. Remote - SSH 配置

    * 内网访问

    按照如下格式填写服务器相关配置信息

    ```bash
    Host 服务器名称（任意取）
        HostName 服务器 IP
        User 登录用户名
        Port SSH 端口
    # 填写好之后，保存关闭文件。
    ```

    * 外网访问

    外网访问服务器需要使用中转服务器作为跳板机。主要在 SSH 配置中添加中转服务器信息，按照如下格式填写相关配置信息
    ```bash
    Host 中转机名称（Jump_Internet）
        HostName 中转机 IP
        User 中转机用户名
        port 中转机端口
        IdentityFile 密钥文件路径（若无密钥，注释本行）

    Host 目标服务器名称（任意取）
        HostName 服务器 IP
        User 登录用户名
        Port SSH端口
        ProxyJump 中转机（Jump_Internet）
    # 填写好之后，保存关闭文件。
    ```

4. 点击远程连接的图标，选择 "Connect to Host in New Window" or "Connect to Host in Current Window"

    ![](/post_img/remote-ssh/remote-ssh4.png)

5. 输入登录密码即可连接远程服务器

    ![](/post_img/remote-ssh/remote-ssh5.png)

    需要注意的是，外网连接过程中会弹出两次密码验证框，第一次为中转机相关的密码，第二次为目标服务器登录密码。

正常情况下，经过上述操作即可在 VSCode 连接到服务器，打开服务器上的项目即可进行调试运行程序。

> 若服务器网络不畅，可能无法在服务器顺利安装。可根据下方内容重新安装VSCode Server。

### 离线安装VSCode Server

离线安装分为两种情况：

1. 本地客户端可以连接 Internet，服务器网络不畅或不能连接 Internet

    在这种情况下，我们可以设置 Remote.SSH（在本地下载VSCode Server，然后将其传输到服务器），操作如下：

    按组合键`Ctrl+,`来打开设置，搜索 `Remote.SSH: Local Server Download` 选择 `always`

    然后执行上节步骤4-5，`Remote - SSH` 会自动在本地下载VSCode Server并使用 SCP 将其传输到服务器

2. 本地客户端和服务器处于同一局域网，但都不能连接 Internet

    VSCode Server默认安装在个人用户目录 $HOME 下的 .vscode-server 文件夹，里面有三个目录 bin、extensions、data。bin 目录下面存放的是VSCode Server程序，extensions 目录下是VSCode Server端安装的插件，data 目录下是用户数据。下面给出离线安装方法。

    从本地 VSCode 的 “帮助-关于” 中获取 commit_id 信息：

    ![](/post_img/remote-ssh/remote-ssh6.png)

    在其他能够联网的主机上，用红框中的信息替换下面链接的 ${commit_id} 得到下载链接，下载 vscode-server-linux-x64.tar.gz

    ```bash
    https://update.code.visualstudio.com/commit:${commit_id}/server-linux-x64/stable
    #例：
    https://update.code.visualstudio.com/commit:6c3e3dba23e8fadc360aed75ce363ba185c49794/server-linux-x64/stable
    ```

    打开本地终端，解压并传输文件到服务器的安装目录，也可以使用文件传输工具（WinSCP，FileZilla）
    ```bash
    # 在本地解压缩,得到 vscode-server-linux-x64
    tar -zxf vscode-server-linux-x64.tar.gz
    # 将已解压程序传输到服务器对应安装目录
    scp -r -P port path-to/vscode-server-linux-x64/* username@server_ip:$HOME/.vscode-server/bin/${commit_id}
    #例：
    scp -r -P 8088 vscode-server-linux-x64/* root@192.168.10.125:/root/.vscode-server/bin/6c3e3dba23e8fadc360aed75ce363ba185c49794
    ```

### 在VSCode Server上安装插件

为了更加方便的调试和运行代码，建议在服务器容器内的VSCode Server里安装 Python 和 Pylance 插件

![](/post_img/remote-ssh/remote-ssh7.png)

> 若服务器网络不畅，可能无法顺利安装插件。可以按照下述方法在服务器VSCode Server里离线安装插件。

1. 以Python插件为例，使用能够联网的本地客户端在[VSCode插件市场](https://marketplace.visualstudio.com/)下载扩展插件Python

    ![](/post_img/remote-ssh/remote-ssh8.png)

2. 利用传输工具或终端将插件传输到服务器，终端传输命令可参考上方 SCP 用法

    > 也可以直接将本地客户端上的插件文件拖拽到 VSCode 的远程项目目录
    ![](/post_img/remote-ssh/remote-ssh9.png)

3. 根据下图指示从 VSIX 安装，找到传输到服务器上的插件以完成安装

    ![](/post_img/remote-ssh/remote-ssh10.png)

部分内容参考自[丰臣英俊-VScode实现本地与远端同步开发的两种方式](https://zhuanlan.zhihu.com/p/592665950)
