## News
---
* 02/2020: One paper is accepted by **Signal Processing**

## Publications
---
* Hongyan Wang, **Helei Qiu***, Wenshu Li. Nonconvex Dictionary Learning Based Visual Tracking Method, _Signal Processing_, 2020 [PDF](https://arxiv.org/abs/2201.02849)
* Hongyan Wang*, **Helei Qiu**, Jia Zheng, Bingnan Pei. Visual Tracking Method Based on Reverse Sparse Representation under Illumination Variation, _Journal of Electronics and Information Technology_, 2019
* Hongyan Wang*, **Helei Qiu**, Tengda Pei. Visual Tracking Method Using Discriminant Dictionary Learning, _Journal of Xidian University_, 2019
* Hongyan Wang*, **Helei Qiu**, Jia Zheng, Bingnan Pei. Visual Tracking Method Based on Low Rank Sparse Representation under Illumination Variation, _Journal of Jilin University (Engineering and Technology Edition)_, 2019